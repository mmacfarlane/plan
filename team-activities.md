Maintaining a list of games & activities we can do as a team, remotely. Along with all the relevant details on how to run/setup. 

| Game/Activity   | # of participants | Owner / Facilitator (s) | Notes                                                        |
|-----------------|-------------------|-------------------------|--------------------------------------------------------------|
| Team Fortress 2 | Up to 32          | John Hope               | Requires a windows machine, bootcamp or < Catalina Mac        |
| Jackbox         | 6-12              | Mario de la Ossa        | A single person hosts, everyone else participates via mobile |
| [Drawful 2](https://jackboxgames.com/drawful-two/)         | 4-8               | Mario de la Ossa        | A single person hosts, everyone else participates via mobile |

