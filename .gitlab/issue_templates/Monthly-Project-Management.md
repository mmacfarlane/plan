## Important Dates to track against:

Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

4th of the Month:

* Draft of the issues that will be included in the next released (released 22nd of next month).
* Start capacity and technical discussions with engineering/UX.

12th of the Month:

* Release scope is finalized. In-scope issues marked with milestone
* Kickoff document is updated with relevant items to be included.

15th of the Month:

* Group Kickoffs calls recorded and uploaded by the end of the day.

## Group Themes for the Release 

Instead of a long list of issues/epics we plan to work on, each Plan group will outline 1 - 3 themes to focus on for the Release. 
Ideally, we will get 1 FE + 2 BE allocated to each theme with the goal of iterating and delivering as much as possible towards that theme.

## Project Management (Gabe) | [Kickoff Video]()

### Group Theme 
Write a quick explanation on what the Theme is, who it is targeted at, and why it is important/a priority for the group.

- [Issue](1)
- [Issue](2)
- [Issue](3)

### Planning Hygiene Checklist

- [ ] One main deliverable plus one spike, or exception described below.
   - _Describe exception here or delete_
- [ ] Deliverables fit within 75% of [3-month rolling average](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning) capacity.

/assign @gweaver @mushakov
/label ~"devops::plan" ~"group::project management"

