## Important Dates to track against:

Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

4th of the Month:

* Draft of the issues that will be included in the next released (released 22nd of next month).
* Start capacity and technical discussions with engineering/UX.

12th of the Month:

* Release scope is finalized. In-scope issues marked with milestone
* Kickoff document is updated with relevant items to be included.

15th of the Month:

* Group Kickoffs calls recorded and uploaded by the end of the day.

## Group Themes for the Release 

Instead of a long list of issues/epics we plan to work on, each Plan group will outline 1 - 3 themes to focus on for the Release. 
Ideally, we will get 1 FE + 2 BE allocated to each theme with the goal of iterating and delivering as much as possible towards that theme.

## Product Planning (Amanda) | [Kickoff Video]()

### Group Theme 
Write a quick explanation on what the Theme is, who it is targeted at, and why it is important/a priority for the group.

- [Issue](#)
- [Issue](#)
- [Issue](#)

### Security 

Pick issues from the [security backlog](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=security&label_name[]=group::product+planning) to keep counts manageable 

- [Issue](#)
- [Issue](#)

### Quality 

- Pick issues from the [bug backlog](https://gitlab.com/groups/gitlab-org/-/issues?sort=title_desc&state=opened&label_name[]=group::product+planning&label_name[]=type::bug&label_name[]=severity::2) to address S2 issues in a timely manner. 
- Review the [error budgets](https://dashboards.gitlab.net/d/stage-groups-product_planning/stage-groups-product-planning-group-dashboard?orgId=1) to determine if work needs to be prioritized to meet our goals.

- [Issue](#)
- [Issue](#)


### EM Tasks

See [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline):

- [ ] `By 5th` Open weighting issue and ask for estimations from the team ([example](https://gitlab.com/gitlab-org/plan/-/issues/243))
  - [ ] BE
  - [ ] FE
  - [ ] Communicate any changes to the weighting process since the last time
    - [ ] BE
    - [ ] FE
- [ ] `By 12th` Complete the estimation process, gather final weights
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Suggest performance, tech debt & bugs (especially check those close to [missing SLO](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning?widget=13864633&udv=949595))
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Use the build board to estimate carry-over (total weight left of ~"workflow::in dev" & half of ~"workflow::in dev" is usually a good start)
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Estimate capacity by taking an average of the last ~3 months (See: [Historical capacity](https://about.gitlab.com/handbook/engineering/development/dev/plan/product-planning/#historical-capacity))
- [ ] `By 14th` Work with PM to fit scope to available capacity (considering carry-over)
- [ ] `By 17th` @mushakov & @johnhope complete the [Planning Hygiene Checklist](#planning-hygiene-checklist)
- [ ] `18th` Kickoff!
- [ ] Gather retrospective feedback on estimation from Engineers and PMs
- [ ] Make any updates to team page/process & close issue

### Planning Hygiene Checklist

- [ ] One main deliverable plus one spike, or exception described below.
   - _Describe exception here or delete_
- [ ] Deliverables fit within 75% of [3-month rolling average](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning) capacity.

### Work Type Classification

Please review the [MR Types dashboard](https://about.gitlab.com/handbook/engineering/metrics/dev/plan/product-planning/#mr-types-dashboard). Check off when complete.
  * [ ] @kushalpandya (EM)
  * [ ] @amandarueda (PM: Product Planning)
  * [ ] @at.ramya (Quality)
  * [ ] @badnewsblair (UX)

/assign @mushakov @johnhope @amandarueda @uhlexis @kushalpandya @at.ramya 
/label ~"devops::plan" ~"group::product planning"

