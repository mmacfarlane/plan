## Certify (Matthew) | [Kickoff Video]()

Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

4th of the Month:

* Draft of the issues that will be included in the next released (released 22nd of next month).
* Start capacity and technical discussions with engineering/UX.

12th of the Month:

* Release scope is finalized. In-scope issues marked with milestone
* Kickoff document is updated with relevant items to be included.

15th of the Month:

* Group Kickoffs calls recorded and uploaded by the end of the day.

### Release Theme

- 

### New Development

- 

### Security/Maintenance/Bugs

- 

### Technical Spikes

- 

### Stretch Goals

- 

### EM Tasks

See [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline):

- [ ] `By 5th` Open weighting issue and ask for estimations from the team ([example](https://gitlab.com/gitlab-org/plan/-/issues/243))
  - [ ] BE
  - [ ] FE
  - [ ] Communicate any changes to the weighting process since the last time
    - [ ] BE
    - [ ] FE
- [ ] `By 12th` Complete the estimation process, gather final weights
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Suggest performance, tech debt & bugs (especially check those close to [missing SLO](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning?widget=13864633&udv=949595))
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Use the build board to estimate carry-over (total weight left of ~"workflow::in dev" & half of ~"workflow::in dev" is usually a good start)
  - [ ] BE
  - [ ] FE
- [ ] `By 12th` Estimate capacity by taking an average of the last ~3 months (See: [Historical capacity](https://about.gitlab.com/handbook/engineering/development/dev/plan/certify/#historical-capacity))
- [ ] `By 14th` Work with PM to fit scope to available capacity (considering carry-over)
- [ ] `By 17th` @mmacfarlane & @jarka complete the [Planning Hygiene Checklist](#planning-hygiene-checklist)
- [ ] `18th` Kickoff!
- [ ] Gather retrospective feedback on estimation from Engineers and PMs
- [ ] Make any updates to team page/process & close issue

### Planning Hygiene Checklist

- [ ] One main deliverable plus one spike, or exception described below.
   - _Describe exception here or delete_
- [ ] Deliverables fit within 75% of [3-month rolling average](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning) capacity.

### Work Type Classification

Please review the [MR Types dashboard](https://about.gitlab.com/handbook/engineering/metrics/dev/plan/product-planning/#mr-types-dashboard). Check off when complete.
  * [ ] @jarka (EM)
  * [ ] @mmacfarlane (PM: Certify)
  * [ ] @at.ramya (Quality)
  * [ ] @danmh (UX)

/assign @mushakov @johnhope @mmacfarlane @danmh @jarka @at.ramya 
/label ~"devops::plan" ~"group::certify"

