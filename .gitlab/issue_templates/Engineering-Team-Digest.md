<!---
Update title to: `Plan Engineering Team Weekly Digest - WE [DATE]`
--->

_Please remove yourself from the assignees list after you have read the announcements._

## :book: Please Review

- [Engineering Week In Review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit)
- [Plan Weekly Meeting Agenda](https://docs.google.com/document/d/1cbsjyq9XAt9UYLIxDq5BYFk47VA5aaTeHfkY2dttqfk/edit) ([Recording]())

## :postal_horn: Announcements & Requests for Feedback

*

## :dancers: Team Availability

## :lifter: Training

## :sun_with_face: Family & Friends Day

Upcoming Family and Friends Days (See [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/88954)):

*

## :crossed_flags: Feature Flags

## :cactus: Other

*

/assign @donaldcook @johnhope @jlear
<!---
Uncomment this out when digest is ready
```
/assign @kushalpandya @rajatgitlab @fguibert @euko @cngo @psimyn @ntepluhina @jprovaznik @jarka @egrieff @cablett @felipe_artur @mcelicalderonG @digitalmoksha @engwan @acroitor
```
--->
