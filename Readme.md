### Monthly Planning Issues

The monthly planning issues are generated from templates in the ./gitlab
directory, named:

* Monthly-Product-Planning.md
* Monthly-Project-Management.md

#### Changing the day Planning issues are generated

1. Go to CI/CD > Schedules
1. Edit the schedule named "Generate monthly planning issues"
1. Change the cron specification to match the new interval

Ensure that the `$PLANNING_ISSUES` envvar is still set.

#### Changing the content of planning issues

Planning issues are generated verbatim from templates, so it's as simple as
editing those. As with any template, quick actions are available and the
generation script doesn't add anything so you can use those to add assignees,
set a due date, @mention others, etc.

#### Known issues

The planning issues are always created with `@johnhope` as the author. This is
because using the project access token triggers Spam detection and the issues
fail to be created, so a personal access token has to be used instead.
