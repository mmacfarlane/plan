require 'httparty'

class PlanningIssueGenerator
  # group_name is just passed from arguments so this class has to transform it
  #   project_management
  def initialize(group_name:)
    @group_name = group_name
  end

  def to_json
    {
      title: "Plan:#{human_group_name} | #{upcoming_milestone['title']} Release Planning",
      description: description_text
    }
  end

  def description_text
    file = File.expand_path("../../.gitlab/issue_templates/#{group_template_filename}", __FILE__)

    unless File.exists?(file)
      raise RuntimeError.new("Expected a file called #{group_template_filename} to exist, but it doesn't. Try creating it and running again.")
    end

    File.read(file)
  end

  private

  def upcoming_milestone
    @upcoming_milestone ||= begin
                              url = "https://gitlab.com/api/v4/groups/9970/milestones?state=active&per_page=100"
                              headers = { 'PRIVATE-TOKEN' => ENV['PLAN_PROJECT_TOKEN'] }
                              response = HTTParty.get(url, headers: headers).parsed_response

                              current_milestone = response.detect do |m|
                                (Date.parse(m['start_date'])..Date.parse(m['due_date'])).include?(Date.today) &&
                                  m['title'] =~ /\A\d{1,2}\.\d{1,2}\z/
                              end

                              date = Date.parse(current_milestone['due_date']) + 1

                              response.detect do |m|
                                (Date.parse(m['start_date'])..Date.parse(m['due_date'])).include?(date) &&
                                  m['title'] =~ /\A\d{1,2}\.\d{1,2}\z/
                              end
                            end
  end

  def human_group_name
    @group_name.split('_').map(&:capitalize).join(' ')
  end

  def group_template_filename
    'Monthly-' + @group_name.split('_').map(&:capitalize).join('-') + '.md'
  end
end
